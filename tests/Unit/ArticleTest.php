<?php

namespace Tests\Unit;

use App\Article;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessGetArticles()
    {
        $response = $this->json('get', route('articles.index'), $this->article->toArray());
        $response->assertStatus(200);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessCreateArticle()
    {
        $this->actingAs($this->user);
        $data = [
            'title' => $this->faker->word,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json('post', route('articles.store', $data));
        $response->assertCreated();
        $this->assertDatabaseHas('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertEquals($data['title'], $resp->data->title);
        $this->assertEquals($data['body'], $resp->data->body);
        $this->assertEquals($data['user_id'], $resp->data->user->id);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testFailedCreateArticleIfUserNotAuth()
    {
        $data = [
            'title' => $this->faker->word,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json('post', route('articles.store', $data));
        $response->assertStatus(401);
        $this->assertDatabaseMissing('articles', $data);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testTitleValidation()
    {
        $this->actingAs($this->user);
        $data = [
            'title' => '',
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json('post', route('articles.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('title', (array)$resp->errors));
        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testBodyValidation()
    {
        $this->actingAs($this->user);
        $data = [
            'title' => $this->faker->word,
            'body' => '',
            'user_id' => $this->user->id
        ];
        $response = $this->json('post', route('articles.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertFalse(array_key_exists('title', (array)$resp->errors));
        $this->assertTrue(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessDestroyArticle()
    {
        $this->actingAs($this->user);
        $article = factory(Article::class)->create([
            'user_id' => $this->user->id
        ]);

        $response = $this->json('delete', route('articles.destroy', $article));
        $response->assertNoContent();
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testFiledDestroyArticleIfUserNotAuth()
    {
        $article = factory(Article::class)->create([
            'user_id' => $this->user->id
        ]);

        $response = $this->json('delete', route('articles.destroy', $article));
        $response->assertStatus(401);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessShowArticle()
    {
        $this->actingAs($this->user);
        $response = $this->json('get', route('articles.show', ['article' => $this->article]));
        $response->assertStatus(200);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testFailedShowArticleIfUserNotAuth()
    {
        $response = $this->json('get', route('articles.show', ['article' => $this->article]));
        $response->assertStatus(401);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessUpdateArticle()
    {
        $this->actingAs($this->user);
        $data = [
            'title' => 'Update title',
            'body' => 'Update content',
            'user_id' => $this->user->id
        ];
        $response = $this->json('put',
            route('articles.update', ['article' => $this->article]), $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('articles', $data);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testFailedUpdateArticleIfUserNotAuth()
    {
        $data = [
            'title' => 'Update title',
            'body' => 'Update content',
            'user_id' => $this->user->id
        ];
        $response = $this->json('put',
            route('articles.update', ['article' => $this->article]), $data);
        $response->assertStatus(401);
        $this->assertDatabaseMissing('articles', $data);
    }
}
