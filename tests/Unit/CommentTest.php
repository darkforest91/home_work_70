<?php

namespace Tests\Unit;

use Tests\TestCase;

class CommentTest extends TestCase
{
    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testSuccessGetComments()
    {
        $response = $this->json('get', route('comments.index'), $this->comments->toArray());
        $response->assertStatus(200);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testSuccessCreateComment()
    {
        $this->actingAs($this->user);
        $comment = [
            'body' => 'test comment',
            'article_id' => $this->article->id,
            'user_id' => $this->user->id,
        ];
        $response = $this->json('post', route('comments.store', $comment));
        $response->assertCreated();
        $this->assertDatabaseHas('comments', $comment);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testFailedCreateCommentIfUserNotAuth()
    {
        $comment = [
            'body' => 'test comment',
            'article_id' => $this->article->id,
            'user_id' => $this->user->id,
        ];
        $response = $this->json('post', route('comments.store', $comment));
        $response->assertStatus(401);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testSuccessShowComment()
    {
        $this->actingAs($this->user);
        $response = $this->json('get', route('comments.show', ['comment' => $this->comments->first()]));
        $response->assertStatus(200);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testFailedShowCommentIfUserNotAuth()
    {
        $response = $this->json('get', route('comments.show', ['comment' => $this->comments->first()]));
        $response->assertStatus(401);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testSuccessDeleteComment()
    {
        $this->actingAs($this->user);
        $response = $this->json('delete', route('comments.destroy', ['comment' => $this->comments->first()]));
        $response->assertNoContent();
        $response->assertStatus(204);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testFiledDeleteCommentIfUserNotAuth()
    {
        $response = $this->json('delete', route('comments.destroy', ['comment' => $this->comments->first()]));
        $response->assertStatus(401);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testSuccessUpdateComment()
    {
        $this->actingAs($this->user);
        $comment = $this->comments->first();
        $data = [
            'body' =>'Update content',
            'user_id' => $this->user->id,
            'article_id' => $this->article->id
        ];
        $response = $this->json('put',
            route('comments.update', ['comment' => $comment]), $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('comments', $data);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testFailedUpdateCommentIfUserNotAuth()
    {
        $comment = $this->comments->first();
        $data = [
            'body' =>'Update content',
            'user_id' => $this->user->id,
            'article_id' => $this->article->id
        ];
        $response = $this->json('put',
            route('comments.update', ['comment' => $comment]), $data);
        $response->assertStatus(401);
        $this->assertDatabaseMissing('comments', $data);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testBodyValidation()
    {
        $this->actingAs($this->user);
        $data = [
            'body' => '',
            'article_id' => $this->article->id,
            'user_id' => $this->user->id
        ];
        $response = $this->json('post', route('comments.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('articles', $data);
        $content = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$content));
        $this->assertTrue(array_key_exists('errors', (array)$content));
        $this->assertTrue(array_key_exists('body', (array)$content->errors));
        $this->assertFalse(array_key_exists('article_id', (array)$content->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$content->errors));
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testArticleIdValidation()
    {
        $this->actingAs($this->user);
        $data = [
            'body' => 'test content',
            'article_id' => '',
            'user_id' => $this->user->id
        ];
        $response = $this->json('post', route('comments.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('articles', $data);
        $content = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$content));
        $this->assertTrue(array_key_exists('errors', (array)$content));
        $this->assertFalse(array_key_exists('body', (array)$content->errors));
        $this->assertTrue(array_key_exists('article_id', (array)$content->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$content->errors));
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testUserIdValidation()
    {
        $this->actingAs($this->user);
        $data = [
            'body' => 'test content',
            'article_id' => $this->article->id,
            'user_id' => ''
        ];
        $response = $this->json('post', route('comments.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('articles', $data);
        $content = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$content));
        $this->assertTrue(array_key_exists('errors', (array)$content));
        $this->assertFalse(array_key_exists('body', (array)$content->errors));
        $this->assertFalse(array_key_exists('article_id', (array)$content->errors));
        $this->assertTrue(array_key_exists('user_id', (array)$content->errors));
    }
}
