<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'article_id' => rand(1,152),
        'user_id' => rand(1,3),
        'body' => $faker->paragraph
    ];
});
