<?php

namespace Tests;

use App\Article;
use App\Comment;
use App\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseTransactions;

    protected $faker, $user, $comments, $article;

    public function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create();
        $this->user = factory(User::class)->create();
        $this->article = factory(Article::class)->create([
            'user_id' => $this->user->id
        ]);
        $this->comments = factory(Comment::class, 3)->create([
            'user_id' => $this->user->id,
            'article_id' => $this->article->id
        ]);
    }
}
